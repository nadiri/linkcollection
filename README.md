# eZ Publish / SF Basics #

## CheatSheets ##
* [General](http://www.symfony2cheatsheet.com/)
* [Symfony CLI](http://overapi.com/static/cs/symfony2-console_en_v1-1.pdf)
* [Twig Overview](http://blog.lazycloud.net/wp-content/uploads/Twig-CheatSheet.pdf)
* [Twig](https://github.com/omansour/stuff/blob/master/cheat-sheets/cheat-sheets-symfony2-twig.pdf)
* [Doctrine](http://www.elao.com/fr/blog/symfony-2-doctrine-2-cheat-sheets)
* [Forms](https://andreiabohner.files.wordpress.com/2011/04/forms.pdf)

## eZ 5 Basics ##
### Console ###
* [ezpublish/console commands](https://github.com/dspe/ez5_cheatsheet/blob/master/eng/command.md)

### Installation ###
* [directory structure short overview](http://partialcontent.com/code/working-with-ez-publish-5-installation)

### General ###
* [slideshare basic intro](http://de.slideshare.net/dfritschy/learnings-from-real-e-z-publish-5-projects)
* [wording doc.ez.no](https://doc.ez.no/pages/viewpage.action?pageId=2720567)

### Development ###
* [how to add field types from doc.ez.no](https://doc.ez.no/display/EZP/eZ+Publish+5+Field+Type+Tutorial)

## Symfony ##
### Best Practices ###
* [Business Logic, Templates, Forms from symfony.com](http://symfony.com/pdf/Symfony_best_practices_2.6.pdf?v=4)
* [Reusability Symfony Cookbook](http://symfony.com/doc/current/cookbook/bundles/best_practices.html#index-1)

### Twig ###
* [The twig book Sensiolabs](http://twig.sensiolabs.org/pdf/Twig.pdf)

## Admin Stuff ##
#### Varnish Config ###
* [Varnish config](https://doc.ez.no/display/EZP/Using+Varnish)